<?php

error_reporting(E_ALL);

define("DS", DIRECTORY_SEPARATOR);
define("FCPATH", __DIR__ . DS);
define("APPPATH", FCPATH . "app" . DS);
define("CPATH", APPPATH . "Controllers" . DS);
define("VPATH", APPPATH . "Views" . DS);
define("MPATH", APPPATH . "Models" . DS);
define("UPLOADPATH", FCPATH . "uploads" . DS);

require_once(realpath(APPPATH . DS . "Loader.php"));

function show_404() {
  $a = [];
  $a[] = "<html>";
  $a[] = "<head></head>";
  $a[] = "<body style=\"font-family: monospace; background-color: #fff; color: #666; text-align: center;\">";
  $a[] = "<h1 style='margin-top: 50px; font-weight: normal;'>--- 404 ---</h1>";
  $a[] = "<p>Page not found</p>";
  $a[] = "<div style='margin-top: 30px;'><a href='/' style='text-decoration:none; color: inherit; border: 1px dashed #888; padding: 5px 15px;'>Click To Return Home</a></div>";
  $a[] = "</body>";
  $a[] = "</html>";
  return implode("", $a);
}

$app = new App();
