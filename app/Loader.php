<?php

require_once APPPATH . "Config.php";
require_once APPPATH . "Utils.php";
require_once APPPATH . "Routes.php";
require_once APPPATH . "Router.php";
require_once APPPATH . "Database.php";
require_once APPPATH . "App.php";
require_once APPPATH . "Controller.php";
require_once APPPATH . "Controller_admin.php";
require_once APPPATH . "Model.php";
require_once APPPATH . "Libraries" . DS . "Auth.php";
require_once APPPATH . "Libraries" . DS . "Validator.php";
require_once APPPATH . "Libraries" . DS . "Rules.php";
require_once APPPATH . "Libraries" . DS . "Validation_errors.php";
require_once APPPATH . "Libraries" . DS . "File_uploader.php";
require_once APPPATH . "Libraries" . DS . "Img.php";
