<?php

class Routes {

  /**
   * Записи роутера
   * Note: можно в записи роутов оставлять slashes в начале и конце
   * @var arrray 
   */
  public static $routes = [
      "/::C_home/index",
      "task::C_home/task",
      "accept-data::C_comment/accept_data",
      "login::C_login/login",
      "logout::C_login/logout",
      "admin::C_admin/admin",
      "ajax/toggle-approved::C_admin_ajax/toggle_approved",
      "ajax/update-comment::C_admin_ajax/update_comment",
  ];

}
