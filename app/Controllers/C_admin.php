<?php

class C_admin extends Controller_admin {

  public function admin() {

    $data = [];
    $data['meta']['title'] = "DL: administrator zone";

    $sort_by = Utils::get_sort_param();

    $mdl = $this->model("M_public");
    $comments_data = $mdl->get_comments($sort_by);

    $arr = [];
    foreach ($comments_data as $c) {
      $arr[] .= $this->view("chunks/single_comment_admin", $c);
    }

    $arr[] = $this->view("chunks/admin_bottom");
    $html = implode("", $arr);

    $data['content'] = $html;

    echo $this->view("template_admin", $data);
  }

}
