<?php

class C_admin_ajax extends Controller_admin {

  public function toggle_approved() {
    if (empty($_POST['id'])) {
      http_response_code(500);
      exit();
    }
    $id = Utils::encode_plain_string($_POST['id']);
    $mdl = $this->model("M_admin");
    $comment_data = $mdl->get_comment_by_id($id);
    $current_approved_status = intval($comment_data['approved']);
    $new_approved_status = ($current_approved_status === 1) ? 0 : 1;
    $mdl->set_approved_status($id, $new_approved_status);
    echo $new_approved_status;
  }

  public function update_comment() {
    if (
            empty($_POST['id']) ||
            empty($_POST['comment'])
    ) {
      http_response_code(500);
      exit();
    }

    $id = Utils::encode_plain_string($_POST['id']);
    $comment = Utils::encode_plain_string($_POST['comment']);
    $mdl = $this->model("M_admin");
    $mdl->update_comment($id, $comment);
  }

}
