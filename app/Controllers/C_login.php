<?php

class C_login extends Controller {

  public function login() {

    $data = [];
    $data['meta']['title'] = "DL: login";

    $form = $this->view("chunks/login", $data);
    $data['content'] = $form;

    if (
            isset($_POST['login']) &&
            isset($_POST['password']) &&
            key_exists($_POST['login'], Config::$admin_credentials) &&
            Config::$admin_credentials[$_POST['login']] == $_POST['password']
    ) {
      Auth::set_admin();
      header("Location: /admin");
      exit;
    }
    echo $this->view("template_basic", $data);
  }

  public function logout() {
    if (Auth::is_admin()) {
      Auth::remove_admin();
      $data = [];
      $data['meta']['title'] = "DL: logged out";
      $data['content'] = $this->view("chunks/logout");
      echo $this->view("template_basic", $data);
      return;
    } else {
      header("Location: /login");
    }
  }

}
