<?php

class C_home extends Controller {

  public function task() {
    $data = [];
    $data['meta']['title'] = "Принт-скрин тестового задания";
    $data['content'] = $this->view("chunks/task", $data);
    echo $this->view("template_basic", $data);
  }

  public function index() {

    $data = [];
    $data['meta']['title'] = "Design Lab: тестовое задание на вакансию PHP-программист";

    // тут в форме target= в айфрейм - т.е. прием отработает другой контроллер/метод
    $data['form'] = $this->view("chunks/form", $data);

    $sort_by = Utils::get_sort_param();

    $mdl = $this->model("M_public");
    $comments_data = $mdl->get_comments($sort_by);

    $arr = [];
    foreach ($comments_data as $c) {
      if ($c['approved'] == 0) {
        continue;
      }
      $arr[] .= $this->view("chunks/single_comment", $c);
    }
    $html = implode("", $arr);

    $data['comments'] = $html;

    echo $this->view("template_comments", $data);
  }

}
