<?php

class C_comment extends Controller {

  public function accept_data() {
    if (strtolower($_SERVER['REQUEST_METHOD']) !== "post") {
      http_response_code(500);
      exit("Error");
      return;
    }

    $validator = new Validator();
    $form_is_valid = true;
    if (!$validator->validate("name", "not_empty|too_long|unwanted_symbols")) {
      $form_is_valid = false;
    }
    if (!$validator->validate("email", "not_empty|valid_email|too_long")) {
      $form_is_valid = false;
    }
    if (!$validator->validate("comment", "not_empty|too_long")) {
      $form_is_valid = false;
    }
    if (!$validator->validate_file("myfile", "allowed_types|file_is_too_large")) {
      $form_is_valid = false;
    }
    echo $this->echo_input_error_js_script("name", $validator->get_error("name"));
    echo $this->echo_input_error_js_script("email", $validator->get_error("email"));
    echo $this->echo_input_error_js_script("comment", $validator->get_error("comment"));
    echo $this->echo_input_error_js_script("myfile", $validator->get_error("myfile"));

    if (!$form_is_valid) {
      return;
    }


    // preparing data for database
    $dbdata = [
        "email" => Utils::encode_plain_string($_POST['email']),
        "name" => Utils::encode_plain_string($_POST['name']),
        "comment" => Utils::encode_plain_string($_POST['comment']),
    ];

    // uploading and processing file
    $fu = new File_uploader("myfile");
    $fu->upload();

    if ($fu->is_uploaded()) {

      // adding some data for database
      $dbdata['img_extension'] = $fu->get_file_extension();
    }

    // saving to database
    $mdl = $this->model("M_public");
    $mdl->insert_comment($dbdata);

    // processing uploaded image
    if ($fu->is_uploaded()) {
      $img = new Img($fu->get_uploaded_file_name());
      $img->resize(Config::$comment_img_width, Config::$comment_img_height);
      $img->rename($mdl->get_insert_id());
    }



    #echo "Success!";
    #echo "<script>alert('Success! Your comment added!');</script>";
    echo "<script>window.parent.bbb();</script>";
  }

  private function echo_input_error_js_script(string $fieldname, string $msg) {
    $str = [];
    $str[] = "<script>";
    $str[] = "window.parent.document.getElementById('{$fieldname}_error').innerHTML = '{$msg}';";
    $str[] = "</script>";
    return implode("", $str);
  }

}
