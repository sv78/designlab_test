<?php

/**
 * Примитивный роутер для URI
 */
class Router {

  private $request_uri;
  private $refined_request_uri;
  private $segments;
  private $controller = null;
  private $method = null;

  const ROUTE_DELIMITER = "::";

  public function __construct() {
    $this->request_uri = strtolower(trim(explode("?", $_SERVER['REQUEST_URI'])[0], "/"));
    $this->refined_request_uri = preg_replace("/(\/){2,}/", "/", $this->request_uri);
    $this->segments = explode("/", $this->refined_request_uri);
    $this->go();
  }

  private function go() {
    foreach (Routes::$routes as $route_str) {
      $route_str_arr = explode(self::ROUTE_DELIMITER, $route_str);
      if (sizeof($route_str_arr) !== 2) {
        http_response_code(500);
        exit('[ Invalid record in Router::routes - not given uri or controller/method ]');
      }
      $route_uri = trim($route_str_arr[0], "/");
      if ($route_uri === $this->refined_request_uri) {
        $r = explode("/", $route_str_arr[1]);
        if (sizeof($r) !== 2) {
          http_response_code(500);
          exit('[ Invalid record in Router::routes - not given controller or method ]');
        }
        $this->controller = ucfirst($r[0]) ;
        $this->method = $r[1];
      }
    }
    if ($this->controller === null) {
      http_response_code(404);
      echo show_404();
      exit;
    }
  }

  public function get_controller() {
    return $this->controller;
  }

  public function get_method() {
    return $this->method;
  }

}
