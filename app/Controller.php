<?php

/**
 * Базовый контроллер
 */
class Controller {
  
  protected function view($filepath = null, array $data = null) {
    require_once APPPATH . "View.php";
    $view = new View($filepath, $data);
    return $view->get_view();
  }

  protected function model($modelname = null) {
    $file = MPATH . $modelname . ".php";
    if (!file_exists($file)) {
      http_response_code(500);
      exit("Error: model [{$modelname}] file not found.");
    }
    require_once $file;
    return new $modelname();
  }

}
