<?php

/**
 * Наше приложение
 */
class App {

  private $router;

  public function __construct() {
    session_start();
    $this->router = new Router();
    $controller_class_file = CPATH . $this->router->get_controller() . ".php";
    if (!file_exists($controller_class_file)) {
      http_response_code(500);
      exit("[" . $this->router->get_controller() . "] class file not found.");
    }
    require_once $controller_class_file;
    $class_name = ucfirst($this->router->get_controller());
    // create controller object
    $obj = new $class_name;
    $method_name = $this->router->get_method();
    if (!method_exists($obj, $method_name)) {
      http_response_code(500);
      exit("method [{$this->router->get_method()}] does not exist in [{$this->router->get_controller()}] class.");
    }
    // call controller method
    $obj->$method_name();
  }

}
