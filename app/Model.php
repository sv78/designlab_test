<?php

class Model {

  public $db;
  private $insert_id;

  public function __construct() {
    $this->db = new mysqli(
            Database::$db['host'],
            Database::$db['user'],
            Database::$db['password'],
            Database::$db['name']
    );
    if ($this->db->connect_errno) {
      http_response_code(500);
      exit("Error: database connection error…");
    };
  }

  public function get_insert_id() {
    return $this->db->insert_id;
  }

}
