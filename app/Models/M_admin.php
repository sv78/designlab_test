<?php

class M_admin extends Model {

  public function get_comment_by_id($id) {
    $sql = "SELECT id, comment, approved "
            . "FROM comments "
            . "WHERE id = {$id} "
            . "LIMIT 1"
    ;
    $q = $this->db->query($sql);
    return mysqli_fetch_assoc($q);
  }

  public function set_approved_status($id, $status) {
    $sql = "UPDATE comments "
            . "SET approved = {$status} "
            . "WHERE id = {$id}"
    ;
    $this->db->query($sql);
  }

  public function update_comment($id, $comment) {
    $sql = "UPDATE comments SET "
            . "comment = '{$comment}', "
            . "moderated = 1 "
            . "WHERE id = {$id}"
    ;
    $this->db->query($sql);
  }

}
