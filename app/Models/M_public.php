<?php

class M_public extends Model {

  public function __construct() {
    parent::__construct();
  }

  public function insert_comment($dbdata) {
    if (!empty($dbdata['img_extension'])) {
      $sql = "INSERT INTO comments (email,name,comment,img_extension) VALUES ('{$dbdata['email']}','{$dbdata['name']}','{$dbdata['comment']}','{$dbdata['img_extension']}');";
    } else {
      $sql = "INSERT INTO comments (email,name,comment) VALUES ('{$dbdata['email']}','{$dbdata['name']}','{$dbdata['comment']}');";
    }
    if (!$this->db->query($sql)) {
      http_response_code(500);
      exit($this->db->error);
    }
  }

  public function get_comments($order_field_and_type = "id DESC") {
    $sql = "SELECT * "
            . "FROM comments "
            #. "WHERE approved = 1 "
            . "ORDER BY $order_field_and_type "
            . "LIMIT 100"
    ;
    $q = $this->db->query($sql);
    return mysqli_fetch_all($q, MYSQLI_ASSOC);
  }

}
