<?php

class Utils {

  public static function debug($mixed, bool $to_echo = true) {
    $str = '<pre style="font-size: 12px; background: #eee; border-bottom: 1px solid #e1e1e1; color: #222; margin: 0 0 5px 0; padding: 5px 5px 5px 100px; font-family: monospace;">';
    $str .= print_r($mixed, true);
    $str .= "</pre>";
    if ($to_echo) {
      echo $str;
    }
    return $str;
  }

  public static function encode_plain_string($str) {
    return (string) htmlentities(addslashes(strip_tags(trim($str))));
  }

  public static function decode_plain_string($str) {
    return (string) stripslashes($str);
  }

  public static function encode_html_string($str) {
    return (string) htmlentities(addslashes(trim($str)));
  }

  public static function decode_html_string($str) {
    return (string) html_entity_decode(stripslashes($str));
  }

  public static function get_sort_param() {
    $a = explode("&", $_SERVER['QUERY_STRING']);
    if (sizeof($a) > 0 && (!empty($a[0]))) {
      switch ($a[0]) {
        case "new":
          return "id DESC";
        case "old":
          return "id ASC";
        case "email":
          return "email ASC";
        case "author":
          return "name ASC";
        default:
          return "id DESC";
      }
    }
    return "id DESC"; // default
  }

}
