<?php

class Auth {

  static function is_admin() {
    if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === true) {
      return true;
    }
    return false;
  }

  static function set_admin() {
    $_SESSION['is_admin'] = true;
  }

  static function remove_admin() {
    unset($_SESSION['is_admin']);
  }

}
