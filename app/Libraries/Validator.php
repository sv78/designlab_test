<?php

class Validator {

  private $fields_errors = [];

  public function validate(string $fieldname = null, string $rules_string = null) {
    #\Utils::debug($_POST);
    #\Utils::debug($_FILES);
    if (!isset($_REQUEST[$fieldname])) {
      http_response_code(500);
      exit("Error: key [ {$fieldname} ] not found in request array data");
    }
    $_REQUEST[$fieldname] = trim($_REQUEST[$fieldname], " ");
    $_REQUEST[$fieldname] = trim($_REQUEST[$fieldname], "\n");
    $_REQUEST[$fieldname] = trim($_REQUEST[$fieldname], "\r\n");
    $rules = explode("|", $rules_string);
    if (sizeof($rules) === 0) {
      return true;
    }
    $rules = array_reverse($rules);

    // проверяем наше значение на все установленные к нему правила проверки
    $field_is_valid = true;
    foreach ($rules as $class_method) {
      if (!method_exists("Rules", $class_method)) {
        http_response_code(500);
        exit("Error: method [ {$class_method} ] not found in Rules class");
      }
      $boolean_result = Rules::$class_method($_REQUEST[$fieldname]); // bool
      if ($boolean_result === false) {
        $field_is_valid = false;
        if (key_exists($class_method, Validation_errors::$errors)) {
          $this->fields_errors[$fieldname] = Validation_errors::$errors[$class_method];
        }
      }
    }
    return $field_is_valid;
  }

  public function validate_file(string $fieldname = null, string $rules_string = null) {
    if (!isset($_FILES[$fieldname])) {
      http_response_code(500);
      exit("Error: key [ {$fieldname} ] not found in files array data");
    }
    $rules = explode("|", $rules_string);
    if (sizeof($rules) === 0) {
      return true;
    }
    $rules = array_reverse($rules);

    // проверяем наше значение на все установленные к нему правила проверки
    $file_is_valid = true;
    foreach ($rules as $class_method) {
      if (!method_exists("Rules", $class_method)) {
        http_response_code(500);
        exit("Error: method [ {$class_method} ] not found in Rules class");
      }
      $boolean_result = Rules::$class_method($fieldname); // bool
      if ($boolean_result === false) {
        $file_is_valid = false;
        if (key_exists($class_method, Validation_errors::$errors)) {
          $this->fields_errors[$fieldname] = Validation_errors::$errors[$class_method];
        }
      }
    }
    return $file_is_valid;
  }

  public function get_error(string $fieldname = null): string {
    if (empty($fieldname)) {
      return "";
    }
    if (key_exists($fieldname, $this->fields_errors)) {
      return $this->fields_errors[$fieldname];
    }
    return "";
  }

}
