<?php

class Img {

  const TYPE_JPEG = "image/jpeg";
  const TYPE_GIF = "image/gif";
  const TYPE_PNG = "image/png";

  private $file;
  private $type;

  public function __construct(string $file = null) {
    if (empty($file)) {
      http_response_code(500);
      exit("Error: image file was not given");
    }
    $this->file = $file;
    $this->type = $this->get_image_type();
  }

  public function resize(int $dest_w = 40, int $dest_h = 40) {
    switch ($this->type) {
      case self::TYPE_JPEG:
        $img = imagecreatefromjpeg($this->file);
        break;
      case self::TYPE_PNG:
        $img = imagecreatefrompng($this->file);
        break;
      case self::TYPE_GIF:
        $img = imagecreatefromgif($this->file);
        break;
      default:
        return;
    }
    $current_size = getimagesize($this->file);
    $new_dim = $this->calculate_new_dimensions($current_size[0], $current_size[1], $dest_w, $dest_h);
    $new_img = imagescale($img, $new_dim[0], $new_dim[1]);
    imagedestroy($img);
    unset($img);
    imagejpeg($new_img, $this->file);
    imagedestroy($new_img);
  }

  public function rename(string $new_name_without_extension = null) {
    if (empty($new_name_without_extension)) {
      return;
    }
    if (!file_exists($this->file)) {
      return;
    }

    $pi = pathinfo($this->file);
    $newpath = $pi['dirname'] . DS . $new_name_without_extension . "." . strtolower($pi['extension']);
    rename($this->file, $newpath);
  }

  private function get_image_type() {
    return image_type_to_mime_type(exif_imagetype($this->file));
  }

  private function calculate_new_dimensions(int $current_w, int $current_h, int $dest_w, int $dest_h) {
    $new_size = [$current_w, $current_h];
    // начнем с ширины
    if ($current_w > $dest_w) {
      $ratio = $current_w / $dest_w;
      $new_size[0] = $dest_w;
      $new_size[1] = round($current_h / $ratio);
    }
    // теперь и высоту
    if ($new_size[1] > $dest_h) {
      $ratio = $new_size[1] / $dest_h;
      $new_size[1] = $dest_h;
      $new_size[0] = round($new_size[0] / $ratio);
    }
    return $new_size;
  }

}
