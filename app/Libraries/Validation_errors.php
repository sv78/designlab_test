<?php

class Validation_errors {

  public static $errors = [
      // строковые
      "not_empty" => "поле не должно быть пустым",
      "unwanted_symbols" => "используются недопустимые символы",
      "too_long" => "слишком длинное значение",
      "valid_email" => "указан неверный формат электронной почты",
      // файловые
      "allowed_types" => "неверный тип файла, допустимы типы: jpg, jpeg, png, gif",
      "file_is_too_large" => "слишком большой объем файла (максимум до 2Mb)",
  ];

}
