<?php

/*
 * --------------------------------------------------------------------
 * Набор правил для проверки
 * --------------------------------------------------------------------
 */

class Rules {
  /*
   * --------------------------------------------------------------------
   * Правила для строк
   * --------------------------------------------------------------------
   */

  public static function not_empty(string $str = null): bool {
    return empty($str) ? false : true;
  }

  public static function unwanted_symbols(string $str = null): bool {
    return (preg_match("#[!@\#№$%^&*+'\"=/\\\?<>|:\]\[(){};,]#", $str) === 1) ? false : true;
  }

  public static function too_long(string $str = null): bool {
    return (mb_strlen($str) > 255) ? false : true;
  }

  public static function valid_email(string $str = null): bool {
    if (preg_match("/[^0-9A-Za-z\-\_\!\@\.]/i", $str) === 1) {
      return false;
    }
    if (preg_match_all("#@#", $str) > 1) {
      return false;
    }
    if (preg_match("#^(.+)@(.+)\.(.+)$#", $str) !== 1) {
      return false;
    }
    if (preg_match("/\.{2,}/", $str) === 1) {
      return false;
    }
    if (preg_match("/@\./", $str) === 1) {
      return false;
    }
    return true;
  }

  /*
   * --------------------------------------------------------------------
   * Правила для файлов
   * --------------------------------------------------------------------
   */

  public static function allowed_types(string $file_input_name): bool {
    if ($_FILES[$file_input_name]['error'] > 0) {
      return true;
    }
    $allowed_types = [
        "image/gif",
        "image/png",
        "image/jpeg"
    ];
    return in_array(strtolower($_FILES[$file_input_name]['type']), $allowed_types) ? true : false;
  }

  public static function file_is_too_large(string $file_input_name): bool {
    // 2048000 = 2mb
    #print_r($_FILES);
    if ($_FILES[$file_input_name]['size'] > 2 * 1000 * 1024) {
      return false;
    }
    return true;
  }

}
