<?php

class File_uploader {

  private $file_fieldname = null;
  private $uploaded_file_name = null;
  private $upload_success = false;
  private $appended = false;

  public function __construct(string $fieldname = null) {
    if (empty($fieldname)) {
      http_response_code(500);
      exit("Error: upload file `fieldname` is not given");
    }
    $this->file_fieldname = $fieldname;

    if ($_FILES[$this->file_fieldname]['size'] > 0) {
      $this->appended = true;
    }
  }

  public function upload(): bool {
    if (!$this->appended) {
      return false;
    }

    $this->uploaded_file_name = $this->get_unique_file_name(UPLOADPATH . $_FILES[$this->file_fieldname]['name']);

    if (move_uploaded_file($_FILES[$this->file_fieldname]['tmp_name'], $this->uploaded_file_name)) {
      $this->upload_success = true;
      return true;
    }
    return false;
  }

  public function is_uploaded() {
    return $this->upload_success;
  }

  public function get_uploaded_file_name() {
    if ($this->appended === false OR $this->is_uploaded() === false) {
      return false;
    }
    return $this->uploaded_file_name;
  }

  public function get_file_extension() {
    $pi = pathinfo($this->uploaded_file_name);
    return strtolower($pi['extension']);
  }

  private function get_unique_file_name($path): string {
    if (file_exists($path)) {
      $pi = pathinfo($path);
      $newpath = $pi['dirname'] . DS . $pi['filename'] . "_new" . "." . $pi['extension'];
      return $newpath;
    }
    return $path;
  }

}
