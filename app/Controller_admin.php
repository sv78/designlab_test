<?php

/**
 * Базовый контроллер админский
 */
class Controller_admin extends Controller {

  public function __construct() {
    if (!Auth::is_admin()) {
      http_response_code(404);
      exit(show_404());
    }
  }

}
