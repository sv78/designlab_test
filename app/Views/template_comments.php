<?php defined("FCPATH") or exit("Access not allowed"); ?>
<!DOCTYPE html>
<html lang="ru-RU">
  <?php
  $head = new View("chunks/head", ["meta" => $meta]);
  echo $head->get_view();
  ?>
  <body>

    <?php
    $topbar = new View("chunks/topbar");
    echo $topbar->get_view();
    ?>

    <div class="container" style="max-width: 800px;">

      <h1 class="text-primary text-center">Коментарии</h1>
      <p class="text-center"><small>
          Сортировка: 
          <a href="?author">Автор</a> |
          <a href="?email">E-mail</a> |
          <a href="?new">Новые</a> |
          <a href="?old">Старые</a>
        </small>
      </p>
      <hr>


      <?php
      if (isset($comments)) {
        echo $comments;
      }
      ?>



      <?php
      if (isset($form)) {
        echo $form;
      }
      ?>


    </div>


    <?php
    $footer = new View("chunks/footer");
    echo $footer->get_view();
    ?>


  </body>
</html>