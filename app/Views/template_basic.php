<?php defined("FCPATH") or exit("Access not allowed"); ?>
<!DOCTYPE html>
<html lang="ru-RU">
  <?php
  $head = new View("chunks/head", ["meta" => $meta]);
  echo $head->get_view();
  ?>
  <body>

    <?php
    $topbar = new View("chunks/topbar");
    echo $topbar->get_view();
    ?>

    <div class="container">
      <?php
      if (isset($content)) {
        echo $content;
      }
      ?>
    </div>


    <?php
    $footer = new View("chunks/footer");
    echo $footer->get_view();
    ?>


  </body>
</html>