<?php defined("FCPATH") or exit("Access not allowed"); ?>
<!DOCTYPE html>
<html lang="ru-RU">
  <?php
  $head = new View("chunks/head", ["meta" => $meta]);
  echo $head->get_view();
  ?>
  <body>

    <?php
    $topbar = new View("chunks/topbar");
    echo $topbar->get_view();
    ?>

    <div style="background-color: #68737f; text-align: center; color: #b5c2cf; padding: 2px 10px 5px; font-size: .85em; cursor: default; margin: 0 auto 20px;">
      Administrator zone : posts moderation
    </div>

    <div class="container">
      <?php
      if (isset($content)) {
        echo $content;
      }
      ?>
    </div>


    <?php
    $footer = new View("chunks/footer");
    echo $footer->get_view();
    ?>


  </body>
</html>