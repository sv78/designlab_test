<?php defined("FCPATH") or exit("Access not allowed"); ?>

<h2 class="text-primary text-center">Оставьте свой комметарий:</h2>

<form id="comment-form" action="/accept-data" accept-charset="utf-8" method="post" autocomplete="off" enctype="multipart/form-data" target="i">

  <div class="form-group">
    <label>* Имя</label>
    <input class="form-control" type="text" name="name" autocomplete="off">
    <div class="form-text text-danger small" id="name_error"></div>
  </div>

  <div class="form-group">
    <label>* Email</label>
    <input class="form-control" type="text" name="email" autocomplete="off">
    <div class="form-text text-danger small" id="email_error"></div>
  </div>

  <div class="form-group">
    <label>* Коментарий</label>
    <textarea class="form-control" name="comment"></textarea>
    <div class="form-text text-danger small" id="comment_error"></div>
  </div>

  <div class="form-group">
    <label>Файл</label>
    <input class="form-control-file" type="file" name="myfile">
    <div class="form-text text-danger small" id="myfile_error"></div>
  </div>

  <div class="form-group">
    <button class="btn btn-primary btn-lg btn-block" type="submit">Отправить</button>
  </div>

</form>
<hr>
<p class="text-center text-danger"><b>Внимание:</b> все коментарии проверяются модератором прежде чем появляются в списке коментариев.</p>

<script>
  (function () {
    // creating iframe for our form
    var i = document.createElement('iframe');
    i.name = "i";
    //i.style.width = "100%";
    //i.style.minHeight = "800px";
    i.style.display = "none";
    document.body.appendChild(i);
  })();

  function bbb() {
    //alert("Now clear form!");
    $('#comment-form').find("input[type=text], input[type=file], textarea").val("");
    $('#my_modal').modal({
      //keyboard: false
    })
  }
</script>

<!-- Modal -->
<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Отправлено!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Спасибо! Коментарий отправлен и ждет модерации администратора!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>