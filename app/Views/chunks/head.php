<?php defined("FCPATH") or exit("Access not allowed"); ?>
<head>
  <meta charset="utf-8">
  <title><?= isset($meta['title']) ? $meta['title'] : ""; ?></title>
  <meta name="robots" content="noindex, nofollow">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="apple-mobile-web-app-capable" content="yes"/>

  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <script src="/assets/js/jquery-3.4.1.min.js"></script>
  <!-- <script src="/assets/js/jquery-3.2.1.slim.min.js"></script> -->
  <script src="/assets/js/popper.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
</head>
