<?php
defined("FCPATH") or exit("Access not allowed");
$img = "/uploads/" . $id . "." . $img_extension;
if (!file_exists(UPLOADPATH . $id . "." . $img_extension)) {
  $img = "/uploads/missing.jpg";
}
?>
<div class="row">

  <div class="col-sm-12 col-lg-5">
    <img class="img-thumbnail" src="<?= $img; ?>" alt="Comment" style="width: 100%;">
  </div>

  <div class="col">
    <pre class="lead"><?= Utils::decode_plain_string($comment); ?></pre>

    <small>
      Автор: <b class="text-primary"><?= Utils::decode_plain_string($name); ?></b><br>
      E-mail: <i><a href="mailto:<?= Utils::decode_plain_string($email); ?>"><?= Utils::decode_plain_string($email); ?></a></i><br>

      Добавлен: <?= Date("Y.m.d", $time_created) . " в " . Date("H:i", $time_created); ?>
      <?php
      if ($moderated == "1") {
        ?>
        <br>
        <div style="display: inline-block; position: relative; width: 18px; height: 18px; overflow: hidden; top: 4px;">
          <svg viewBox="0 0 24 24">
          <path fill="#ff0000" d="M21.7,13.35L20.7,14.35L18.65,12.3L19.65,11.3C19.86,11.09 20.21,11.09 20.42,11.3L21.7,12.58C21.91,12.79 21.91,13.14 21.7,13.35M12,18.94L18.06,12.88L20.11,14.93L14.06,21H12V18.94M12,14C7.58,14 4,15.79 4,18V20H10V18.11L14,14.11C13.34,14.03 12.67,14 12,14M12,4A4,4 0 0,0 8,8A4,4 0 0,0 12,12A4,4 0 0,0 16,8A4,4 0 0,0 12,4Z" />
          </svg>
        </div>
        <span class="text-danger">Изменено модератором</span>
        <?php
      }
      ?>
    </small>
  </div>
</div>

<hr>


