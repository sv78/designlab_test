<?php defined("FCPATH") or exit("Access not allowed"); ?>

<h1 class="text-center">Вход админа:</h1>

<div class="container" style="max-width: 400px;">
  <form action="" accept-charset="utf-8" method="post" autocomplete="off" enctype="application/x-www-form-urlencoded">

    <div class="form-group">
      <label>* Login</label>
      <input class="form-control" type="text" name="login" autocomplete="off" autofocus="on">
      <div class="form-text text-danger small" id="login_error"></div>
    </div>

    <div class="form-group">
      <label>* Password</label>
      <input class="form-control" type="text" name="password" autocomplete="off">
      <div class="form-text text-danger small" id="password_error"></div>
    </div>

    <div>
      <button class="btn btn-primary btn-lg btn-block" type="submit">Войти</button>
    </div>

  </form>

</div>