<?php defined("FCPATH") or exit("Access not allowed"); ?>

<!-- Modal -->
<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Редакция коментария</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <textarea class="form-control" id="my_modal_textarea"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id="my_modal_save">Сохранить</button>
      </div>
    </div>
  </div>
</div>



<script>


  window.onload = function () {
    (function () {
      document.addEventListener("click", documentClickListener, false);

      function documentClickListener(e) {
        if (e.target.hasAttribute("data-action-toggle-approved")) {
          toggleApproved(e.target);
        }

        if (e.target.hasAttribute("data-action-edit")) {
          editComment(e.target);
        }

        if (e.target === document.getElementById('my_modal_save')) {
          $('#my_modal').modal('hide');
          var comment_id = document.getElementById('my_modal_textarea').getAttribute("data-comment-id");
          updateComment(comment_id, document.getElementById('my_modal_textarea').value);
        }
      }

      function updateComment(comment_id, str) {
        var request = $.ajax({
          url: "/ajax/update-comment",
          method: "POST",
          // async: false,
          cache: false,
          data: {id: comment_id, comment: str}
        });
        request.done(function (msg) {
          console.log(msg);
          $("#comment_" + comment_id).text(str);
        });
      }

      function toggleApproved(elm) {
        var request = $.ajax({
          url: "/ajax/toggle-approved",
          method: "POST",
          // async: false,
          cache: false,
          data: {id: elm.getAttribute("data-action-toggle-approved")}
        });
        request.done(function (msg) {
          switch (msg) {
            case "1":
              $(elm).addClass("btn-success").text("approved");
              break;
            case "0":
              $(elm).removeClass("btn-success").text("denied");
              break;
          }
        });
      }

      function editComment(elm) {
        var id = elm.getAttribute("data-action-edit");
        var comment = document.getElementById("comment_" + id).innerHTML;
        document.getElementById('my_modal_textarea').value = comment;
        document.getElementById('my_modal_textarea').setAttribute("data-comment-id", id);
        $('#my_modal').modal({
          //keyboard: false
        })
      }

    })();
  };




</script>