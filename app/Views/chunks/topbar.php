<?php defined("FCPATH") or exit("Access not allowed"); ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/">

    <div style="display: inline-block; width: 30px; height: 30px;" class="align-top" alt="Design Lab">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ="0px" y="0px" viewBox="0 0 30 30" xml:space="preserve">
        <g>
          <g>
            <path fill="#ffffff" d="M12.7,8.7v16.4c0,1.2,0,2.6,0.1,3.5h-2.2l-0.1-2.4h-0.1c-0.8,1.5-2.4,2.7-4.6,2.7C2.5,28.9,0,26.2,0,22
                  c0-4.5,2.8-7.3,6.1-7.3c2.1,0,3.5,1,4.1,2.1h0.1V8.7H12.7z M10.2,20.6c0-0.3,0-0.7-0.1-1c-0.4-1.6-1.7-2.9-3.6-2.9
                  c-2.6,0-4.1,2.2-4.1,5.2c0,2.7,1.3,5,4,5c1.7,0,3.2-1.1,3.6-2.9c0.1-0.3,0.1-0.7,0.1-1.1V20.6z"/>
          </g>
          <g>
            <path fill="#ffffff" d="M15.1,1.1h2.4v25.5H30v2H15.1V1.1z"/>
          </g>
        </g>
      </svg>
    </div>

    <!-- <img src="/docs/4.0/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt=""> -->
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <?php
      if (Auth::is_admin()) :
        ?>
        <li class="nav-item">
          <a class="nav-link" href="/admin">Admin</a>
        </li>
        <?php
      endif;
      ?>
      <li class="nav-item active">
        <a class="nav-link" href="/">Коментарии</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/task">Текст задания</a>
      </li>
      <?php
      if (Auth::is_admin()) :
        ?>
        <li class="nav-item">
          <a class="nav-link" href="/logout">Logout</a>
        </li>
        <?php
      else :
        ?>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
      <?php
      endif;
      ?>

    </ul>
    <span class="navbar-text" style="font-size: .75em;">
      PHP-программист Викулов
    </span>
  </div>
</nav>