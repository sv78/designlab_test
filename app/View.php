<?php

class View {

  private $view = "";

  public function __construct($filepath = null, array $data = null) {
    if (empty($filepath)) {
      http_response_code(500);
      exit("Error: view file is not defined.");
    }
    $file = VPATH . strtolower($filepath) . ".php";
    if (!file_exists($file)) {
      http_response_code(500);
      exit("Error: view file [ {$filepath}.php ] does not exist.");
    }
    if (!empty($data)) {
      extract($data);
      unset($data);
    }
    ob_start();
    require $file;
    $this->view = ob_get_clean();
  }

  public function get_view() {
    return $this->view;
  }

}
